var express = require('express');
var router = express.Router();
// var mongoose = require('mongoose');
var Empty = require('../models/emptiness');

/* GET ALL */
router.get('/', function(req, res, next) {
    Empty.find(function (err, ratings) {
    if (err) return next(err);
    res.json(ratings);
  });
});

/* GET BY ID */
router.get('/:id', function(req, res, next) {
    Empty.findById(req.params.id, function (err, ratings) {
    if (err) return next(err);
    res.json(ratings);
  });
});

/* SAVE */
router.post('/', function(req, res, next) {
    Empty.create(req.body, function (err, ratings) {
    if (err) return next(err);
    res.json(ratings);
  });
});

/* UPDATE */
router.put('/:id', function(req, res, next) {
    Empty.findByIdAndUpdate(req.params.id, req.body, function (err, ratings) {
    if (err) return next(err);
    res.json(ratings);
  });
});

/* DELETE  */
router.delete('/:id', function(req, res, next) {
    Empty.findByIdAndRemove(req.params.id, req.body, function (err, ratings) {
    if (err) return next(err);
    res.json(ratings);
  });
});

module.exports = router