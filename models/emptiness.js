var mongoose = require('mongoose')


var Schema = mongoose.Schema;

var emptiness = new Schema({
  title: {
    type: String,
    required: [true, 'Blog title is required']
  },
  summary: String,
  lastModified: String,
  imageUrl: String
});

//Export function to create "SomeModel" model class
module.exports = mongoose.model('emptiness', emptiness );